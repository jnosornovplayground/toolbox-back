const express = require('express')
const cors = require("cors")

const app = express()
const PORT = 3000

const { getInvertedEchoResponse, isPalimdrome } = require('./helpers/echoing')

app.use(cors())
app.use('/iecho', (req, res) => {
  const { text } = req.query

  if (!text) {
    return res.status(400).json({
      error: 'no text'
    })
  }

  const iEcho = getInvertedEchoResponse(text)

  res.json({
    text: iEcho,
    palimdrome: isPalimdrome(iEcho)
  })
})

app.listen(PORT, () => {
  console.log(`Application launched in port ${PORT}`)
})

module.exports = app
