const app = require('../server')
const request = require('supertest')
const chai = require('chai')
const expect = chai.expect

describe('test iEcho API', () => {
  describe('GET /iecho', () => {
    describe('should get a response', () => {
      it('with palimdrome false for text equal test', function (done) {
        const expected = { text: 'tset', palimdrome: false }
        request(app)
          .get('/iecho?text=test')
          .end(function (err, res) {
            expect(res.statusCode).to.equal(200)
            expect(res.body).to.be.an('object')
            expect(res.body).to.deep.equal(expected)
            done()
          })
      })

      it('with palimdrome true for text equals wow', function (done) {
        const expected = { text: 'wow', palimdrome: true }
        request(app)
          .get('/iecho?text=wow')
          .end(function (err, res) {
            expect(res.statusCode).to.equal(200)
            expect(res.body).to.be.an('object')
            expect(res.body).to.deep.equal(expected)
            done()
          })
      })

      it('with an error message for empty text', function (done) {
        const expected = { error: 'no text' }
        request(app)
          .get('/iecho?text=')
          .end(function (err, res) {
            expect(res.statusCode).to.equal(400)
            expect(res.body).to.be.an('object')
            expect(res.body).to.deep.equal(expected)
            done()
          })
      })
    })
  })
})
