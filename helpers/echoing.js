function getInvertedEchoResponse (text) {
  return text.split('').reverse().join('')
}

function isPalimdrome (text) {
  const originalText = text
  const reversedText = getInvertedEchoResponse(text)

  return originalText === reversedText
}

module.exports = {
  getInvertedEchoResponse,
  isPalimdrome
}
